
from math import radians
import cv2
from numpy import *
from RLearning.RLBasic import *
import flappy
import time
import json
import itertools
import sys
import pygame


enable_states_show=True

bird = cv2.Canny(cv2.imread("resources/bird_wing_up.png",0),100,200)
pipe_end = cv2.imread("resources/pipe_end.png",0)
pipe_body = cv2.split(cv2.imread("resources/pipe_body.png",1))
pipe_body_green = pipe_body[2]

font = pygame.font.SysFont("monospace", 10)

def getLocationBird(img):
	res = cv2.matchTemplate(cv2.Canny(img[:, flappy.bird_x:32+flappy.bird_x],100,200), bird, cv2.TM_CCOEFF_NORMED)
	min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
	return max_loc[0]+flappy.bird_x, max_loc[1]

def getLocationPipe(img):
	res = cv2.matchTemplate(img[0:1,:], pipe_body_green[0:1,:], cv2.TM_SQDIFF_NORMED)
	k = res.min()
	if k == 1.0:
		return (1000,1000)
	loc = where(res<=k+0.1)
	minx = 1000
	for pt in zip(*loc[::-1]):
		if pt[0] <= minx :
			minx=pt[0]
	line = img[:,minx+16]
	y = where(line<=1)
	for pt in zip(*y[::-1]):
		return (minx, pt[0])
	return (1000, 1000)

class OA(RLBase):
    def init(self):
        self.biasreward = 0
        self.game = flappy.game(mode)
        self.game.start()

    def dumpBrain(self, filename):
		js = {"statelist":self.statelist.tolist(), "Q":self.Q}
		json.dump(js, open(filename,"w"))

    def loadBrain(self, filename):
		js = json.load(open(filename))
		self.statelist = array(js["statelist"])
		self.Q = js["Q"]

    def BuildActionList(self):
        return arange(0,2)

	def BuildSateList(self):
		states = [[0,0]]
		return array(states)

	def GetReward(self, s):
		t = self.biasreward
		self.biasreward = 0
		if self.game.isAlive():
			return (1+t, False)
		return (-1000, True)
	def DoAction(self, action, x):
		if action == 0:
			self.game.press_jump()
		if mode == flappy.MODE_VISUAL:
			self.game.process_frame()
		else:
			while self.game.process_frame():
				pass
		if mode == flappy.MODE_VISUAL and True:
			b = self.game.get_buffer()
			buffer=pygame.surfarray.pixels3d(b)
			green_channel=buffer[:,:,2].transpose(1,0)
			buffer2=cv2.cvtColor(buffer.transpose((1,0,2)), cv2.COLOR_RGB2GRAY)
			loc = getLocationBird(buffer2)
			loc2 = getLocationPipe(green_channel)
			birdy = loc[1]
			pipey = loc2[1]
			if pipey>=1000:
				pipey = 4*32
			pygame.draw.rect(self.game.get_buffer(), (255,0,0), (loc2[0], loc2[1], 80, 32), 2)
			pygame.draw.rect(self.game.get_buffer(), (255,0,0), (loc[0], loc[1], 32, 32), 1)
			bird_pos = font.render(str(birdy), 1, (0,0,255))
			del buffer, buffer2, green_channel
			self.game.get_buffer().blit(bird_pos, (loc[0], loc[1]-10))

			bx = int(loc[0]/50)
			by = int(loc[1]/50)
			px = int(loc2[0]/50)
			py = int(loc2[1]/50)
			if enable_states_show:
				#pygame.draw.rect(self.game.get_buffer(), (0,0,0), (10, 70, 110, 100), 0)
				pygame.draw.rect(self.game.get_buffer(), (255,255,0), (10+bx*10, 70+by*10, 10, 10), 0)
				if loc2[1]<1000:
					pygame.draw.rect(self.game.get_buffer(), (0,150,0), (10+px*10, 70+py*10, 10, 10), 0)
					pygame.draw.rect(self.game.get_buffer(), (0,150,0), (10+(px+1)*10, 70+py*10, 10, 10), 0)
				for i in range(0, 568/50):
					for j in range(0, 512/50):
						pygame.draw.rect(self.game.get_buffer(), (255,0,0), (10+i*10, 70+j*10, 11, 11), 1)

			if loc2[1]<1000:
				pygame.draw.line(self.game.get_buffer(), (0,0, 255), (loc[0]+16, loc[1]),(loc2[0]+40, loc2[1]+32), 1)
				brick_pos = font.render(str(pipey),1, (0,0,255))
				self.game.get_buffer().blit(brick_pos, (loc2[0]-20, loc2[1]))
			dist = (32+pipey-birdy)
		else:
			dist = (self.game.get_pipes()[0][1]+1)*32-int(self.game.get_bird())
		self.game.render()
		return array([int(dist/10), 0])
	def GetInitialState(self):
		self.init()
		dist = (self.game.get_pipes()[0][1]+1)*32-self.game.get_bird()
		return array([int(dist/10), 0])
	def getStatelistLength(self):
		return self.statelist.shape[0]
	def getGame(self):
		return self.game

def player_ai(maxepisodes):
    if mode == flappy.MODE_FAST:
        maxsteps = 10000
        CP = OA(0.6, 1.0, 0.0)
    else:
        maxsteps = 5000000
        CP = OA(0.0, 1.0, 0.0)
    grafica  = False
    if len(sys.argv) >= 4:
        CP.loadBrain(sys.argv[3])

    CP.epsilon = 0.0
    for j in range(maxepisodes/100):
		for i in range(100):
			total_reward, steps  = CP.QLearningEpisode( maxsteps, grafica )
			print ('Episode: ', i+j*100, '  Steps:' , steps, '  Reward:',
                str(total_reward), CP.game.get_score(), ' epsilon: ',
                str(CP.epsilon), 'n_states: ', CP.getStatelistLength())
		if len(sys.argv)>=3:
			CP.dumpBrain(sys.argv[2])

if __name__ == '__main__':
    if len(sys.argv)>=2:
    	if sys.argv[1]=="V":
    		mode = flappy.MODE_VISUAL
    	elif sys.argv[1]=="F":
    		mode = flappy.MODE_FAST
    	else:
    		print "INVALID MODE - USE F/V"
    		exit(-1)
    else:
    	mode = flappy.MODE_VISUAL

    player_ai(20000)
