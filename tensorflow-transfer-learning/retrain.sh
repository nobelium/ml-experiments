python tensorflow/examples/image_retraining/retrain.py \
--bottleneck_dir=bottlenecks \
--how_many_training_steps 40000 \
--model_dir=model/inception \
--output_graph=model/retrained_graph.pb \
--output_labels=model/retrained_labels.txt \
--image_dir images
