library(caTools)
library(randomForest)
library(readr)
library(data.table)
library(pROC)

data <- read_csv("<data_train>_<part1>.csv")
# Drop index column
data <- data[,-1]

# Mark all categorical columns as factors
data$tag <- factor(data$tag)
#More columns where marked (removed for clarity)

# Split data set into training and test
split = sample.split(data$tag, SplitRatio=0.8)
train = subset(data, split==TRUE)
test = subset(data, split==FALSE)

# Build model
pc <- proc.time()
randomForest = randomForest(tag ~ ., data=train, nodesize=10, ntree=601, do.trace=TRUE)
proc.time() - pc

# Save model to a file
save(randomForest, file = "randomForestModel_<part1>.rda")

# Predict values
randomForestPredict = predict(randomForest, newdata=train)

# Prints out put matrix
randomForestTable = table(train$tag, randomForestPredict)
# Calculate accuracy
sum(diag(randomForestTable))/nrow(train)
