model <- load(“rf_model.rda”)

data <- read_csv("hackathon2015_eval.csv")
dataindex <- data[,1]
# Mark categorical data as factors and drop index column

randomForestPredict = predict(model, newdata=data, type="prob")

predictions <- data.frame(Index=dataindex, Score=randomForestPredict[,2] )

# Final output score file
write_csv(predictions, "rf_benchmark.csv")
