library(rpart)
library(caTools)
library(randomForest)
library(readr)

load("randomForestModel_<part1>.rda")
model <- randomForest_<part1>
# Remove variable to save RAM
rm("randomForest_<part1>")

for(i in [2:10]){
 filename <- calculate file name
 load(filename)

 model <- combine(model, <model_name>_<partn>)
 rm(“<model_name>_<partn>”)
}
# Save model to a file
save(model, file="rf_model.rda")
