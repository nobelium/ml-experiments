***Submission for Amazon TRMS hackathon (Transaction Risk Management System)***

(classification problem)

- Training data files are confidential
- Data contained about 500 feature and just over 8GB of data
- Split it into 8 files and trained 8 random forests
- Merged all the forests
- Area Under ROC Curve was used to rank the teams
- This submission scored 0.983

Ranked 5th (1st score - 0.992)
